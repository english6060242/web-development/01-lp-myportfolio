# Portfolio Website

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](LICENSE)
[![Netlify Status](https://api.netlify.com/api/v1/badges/NETLIFY_SITE_ID_HERE)](https://app.netlify.com/sites/YOUR_SITE_NAME_HERE)

This responsive website showcases my skills, projects, CV, personal description, contact information, and a review section where colleagues or employers can share feedback. It's built using HTML, CSS, and JavaScript, leveraging Webpack for bundling and Firebase for data storage and management.

## Features

- **Skillset Display:** Detailed presentation of my skills and expertise.
- **CV Section:** Personal resume and qualifications.
- **Personal Introduction:** Brief description introducing myself.
- **Project Showcase:** Display of selected projects.
- **User Review System:** Visitors can share reviews, managed by an administrator.
- **Firebase Integration:** Storage and management of reviews with Firebase.

## User Review System

One of the highlights of this project is the review system. Visitors can leave reviews that await approval from an administrator. Admins have the authority to approve, reject, or delete existing approved reviews, effectively curating the content. Firebase is used as the database for managing these reviews.

## Usage

To run the project locally:

1. Clone the repository.
2. Install dependencies using `npm install`.
3. Build the project using `npm run build`.
4. Run the project locally using a live server.

## Contributing

Contributions are welcome! If you'd like to contribute, please fork the repository, create a new branch, make your changes, and submit a pull request.

## Deployment

The website is hosted on Netlify at [Your Netlify Domain](https://lp-myportfolio.netlify.app/).

## License

This project is licensed under the [MIT License](LICENSE).

## Contact

For any inquiries or issues regarding the project, feel free to contact me at [Your Email Address].

Enjoy exploring my portfolio!
