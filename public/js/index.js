import { updateLoginText, handleLoginLogout} from './common';
import { initializeApp } from 'firebase/app';
import { getFirestore, collection, getDocs, addDoc } from 'firebase/firestore';


const firebaseConfig = {
  apiKey: "AIzaSyDTk6YSfVUT3oDM80hof-S_c1oJZM2NFHg",
  authDomain: "reviews2-6db94.firebaseapp.com",
  projectId: "reviews2-6db94",
  storageBucket: "reviews2-6db94.appspot.com",
  messagingSenderId: "211637723531",
  appId: "1:211637723531:web:2490a88fc8578d4fb04f8e"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const lang = document.documentElement.lang;
let colRef;
if(lang === 'en'){colRef = collection(db, 'rawReviews');}
if(lang === 'es'){colRef = collection(db, 'rawSpanishReviews');}
// Adding documents
const reviewForm = document.getElementById("reviewForm");
reviewForm.addEventListener("submit", (e) => {
  e.preventDefault();

  // Get form data
  const name = reviewForm.name.value;
  const phone = reviewForm.phone.value;
  const email = reviewForm.email.value;
  const message = reviewForm.message.value;

  if((name != '')&&(message != '')){
    let isConfirmed;
    if(lang === 'en'){isConfirmed = window.confirm('Are you sure you want to submit this review?');}
    if(lang === 'es'){isConfirmed = window.confirm('¿Enviar reseña?');}
    if (isConfirmed) {
      addDoc(colRef, {
        name,
        phone,
        email,
        message
      })
      .then(() => {
        reviewForm.reset();
        if(lang === 'en'){alert('Review submitted successfully! Your review will be visible in the reviews section after admin approval.');}
        if(lang === 'es'){alert('La reseña se ha enviado con éxito! Su reseña se encontrará visible una vez aprobada por el administrador.');}
      });
    }
  }
  else
  { 
    if(lang === 'en'){alert('You must fill at least the name and message fields');}
    if(lang === 'es'){alert('Debe escribir al menos su nombre y un mensaje');}
  }
});

updateLoginText();
const loginbutton = document.getElementById("authenticate");
loginbutton.addEventListener("click", (event) => {
  handleLoginLogout();
});