import { updateLoginText, handleLoginLogout} from './common';

updateLoginText();
const loginbutton = document.getElementById("authenticate");
loginbutton.addEventListener("click", (event) => {
  handleLoginLogout();
});