import { getIsLoggedIn, updateLoginText, handleLoginLogout, loginOnly} from './common';
import { initializeApp } from 'firebase/app';
import { getFirestore, collection, getDocs, getDoc, deleteDoc, doc, onSnapshot, addDoc } from 'firebase/firestore';

let isLoggedIn = getIsLoggedIn();

updateLoginText();

document.addEventListener('DOMContentLoaded', function() {
  // Check if the browser supports the Notification API
  if ('Notification' in window) {
    // Request permission for notifications if not already granted
    if (Notification.permission !== 'granted') {
      Notification.requestPermission()
        .then(function (permission) {
          if (permission === 'granted') {
            // Create and show the notification when permission is granted
            showNotification('Success', 'Permission granted');
          }
        });
    }
  }
  // Function to create and show the notification
  function showNotification(title, message) {
    // Create a new notification
    var notification = new Notification(title, {
      body: message
    });

    // Handle user interaction with the notification (e.g., clicking it)
    notification.onclick = function () {
      // Do something when the user clicks the notification
      window.focus(); // You can focus on the window or open a specific URL, etc.
      notification.close(); // Close the notification
    }
  }
    
  // Initialize Firebase Configuration
  const firebaseConfig = {
      apiKey: "AIzaSyDTk6YSFVUT3oDM80hof-S_c1oJZM2NFHg",
      authDomain: "reviews2-6db94.firebaseapp.com",
      projectId: "reviews2-6db94",
      storageBucket: "reviews2-6db94.appspot.com",
      messagingSenderId: "211637723531",
      appId: "1:211637723531:web:2490a88fc8578d4fb04f8e"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getFirestore(app);
  const lang = document.documentElement.lang;
  let colRef;
  let colRefRaw;
  if(lang === 'en'){colRef = collection(db, 'reviews');}
  if(lang === 'es'){colRef = collection(db, 'spanishReviews');}
  if(lang === 'en'){colRefRaw = collection(db, 'rawReviews');}
  if(lang === 'es'){colRefRaw = collection(db, 'rawSpanishReviews');}

  // Select reviews-container element in the html in order to add/delete reviews
  const reviewsContainer = document.getElementById("reviews-container");
  // Function to create HTML elements for each review
  let delButtonText;
  if(lang === 'en'){delButtonText = 'Delete';}
  if(lang === 'es'){delButtonText = 'Eliminar';}

  function createReviewElement(id, name, message) {
    isLoggedIn = getIsLoggedIn();
    const reviewElement = document.createElement("div");
    reviewElement.classList.add("review"); 
    if(isLoggedIn) {
      reviewElement.classList.remove("unlogged");
      reviewElement.innerHTML = `
      <h4>${name}</h4>
      <p class="myParagraph add_margin">${message}</p>
      <input class="w-small-100 review_button no_margin " type="submit" value="${delButtonText}" data-review-id="${id}" />
    `;
    }
    else 
    {
      reviewElement.classList.add("unlogged"); 
      reviewElement.innerHTML = `
      <h4>${name}</h4>
      <p class="myParagraph add_margin">${message}</p>
    `;
    }
    return reviewElement;
  }

  const unsubscribe = onSnapshot(colRef, (snapshot) => {
    reviewsContainer.innerHTML = ''; // Clear the existing reviews

    snapshot.forEach((doc) => {
      const data = doc.data();
      const id = doc.id;
      const name = data.name;
      const message = data.message;

      // Create HTML elements for each review and append them to the container
      const reviewElement = createReviewElement(id, name, message);
      reviewsContainer.appendChild(reviewElement);
    });
  });

  const rawReviewsContainer = document.getElementById("raw-reviews-container");

  let approveButtonText, declineButtonText;
  if(lang === 'en'){approveButtonText = 'Approve'; declineButtonText = 'Decline';}
  if(lang === 'es'){approveButtonText = 'Aprobar'; declineButtonText = 'Denegar';}

  function createRawReviewElement(id, name, message) {
    const rawReviewElement = document.createElement("div");
    rawReviewElement.classList.add("review"); 
    rawReviewElement.innerHTML = `
      <h4>${name}</h4>
      <p>${message}</p>
        <div class="raw_button_container">
          <input class="w-small-100 raw_button no_margin" type="submit" value="${approveButtonText}" data-review-id="${id}" />
          <input class="w-small-100 raw_button no_margin" type="submit" value="${declineButtonText}" data-review-id="${id}" />
        </div>  
    `;
    return rawReviewElement;
  }

  const unsubscribeRawReviews = onSnapshot(colRefRaw, updatwRawReviews);

  function updatwRawReviews() {
    if (isLoggedIn) {
      rawReviewsContainer.innerHTML = ''; // Clear the existing raw reviews
  
      getDocs(colRefRaw)
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            const data = doc.data();
            const id = doc.id;
            const name = data.name;
            const message = data.message;
  
            // Create HTML elements for each review and append them to the container
            const reviewElement = createRawReviewElement(id, name, message);
            rawReviewsContainer.appendChild(reviewElement);
          });
        })
        .catch((error) => {
          console.error('Error getting raw reviews: ', error);
        });
    } else {
      rawReviewsContainer.innerHTML = ''; // Clear the existing raw reviews
    }
  }

/*function updatwReviews() {
  rawReviewsContainer.innerHTML = ''; // Clear the existing raw reviews
  
  getDocs(colRef)
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        const data = doc.data();
        const id = doc.id;
        const name = data.name;
        const message = data.message;

        // Create HTML elements for each review and append them to the container
        const reviewElement = createReviewElement(id, name, message);
        ReviewsContainer.appendChild(reviewElement);
      });
    })
    .catch((error) => {
      console.error('Error getting reviews: ', error);
    });   
  }*/

  /*function updateDeleteButtons() {
    const reviewButtons = reviewsContainer.querySelectorAll('.review_button');
    isLoggedIn = getIsLoggedIn();
    if(!isLoggedIn) {
      reviewButtons.forEach(button => {
        button.remove(); // Remove each delete button
      });
    }
    else {

    }*/

  function updateDeleteButtons() {
    const reviewButtons = reviewsContainer.querySelectorAll('.review_button');
    isLoggedIn = getIsLoggedIn();
    if(!isLoggedIn) {
      reviewButtons.forEach(button => {
        button.remove(); // Remove each delete button
      });
      const reviewDivs = reviewsContainer.querySelectorAll('.review');
        reviewDivs.forEach(div => {
            div.classList.add("unlogged");
        });
    }
    else {
      // Select all div elements with the class "review" inside the reviewsContainer
      const reviewDivs = reviewsContainer.querySelectorAll('.review');

      // Loop through each div and append the button HTML
      reviewDivs.forEach(div => {
        const id = div.getAttribute('data-review-id'); // Assuming each div has a data-review-id attribute
        const buttonHTML = `<input class="w-small-100 review_button no_margin" type="submit" value="${delButtonText}" data-review-id="${id}" />`;
        div.innerHTML += buttonHTML;
        div.classList.remove("unlogged");
      });
    }
  }
    
  const loginbutton = document.getElementById("authenticate");
  loginbutton.addEventListener("click", (event) => {
    handleLoginLogout();
    isLoggedIn = getIsLoggedIn();
    updatwRawReviews();
    //window.location.reload(); // Reload the page
    updateDeleteButtons();
  });

  // Function to delete a review
  async function deleteReview(reviewID) {
    try {
      await deleteDoc(doc(colRef, reviewID));
      /* alert("Review deleted successfully!"); */
    } catch (error) {
      if(lang === 'en'){alert("Error, could not delete review");}
      if(lang === 'es'){alert('Error, no se pudo eliminar las reseñas');}
    }
  }
  
  // Your event listener for the delete button
  reviewsContainer.addEventListener("click", (event) => {
    const target = event.target;
    let isConfirmed = false;
    isLoggedIn = getIsLoggedIn();
    if (target && target.matches("input[data-review-id]")) 
    {
      const reviewID = target.getAttribute("data-review-id");
      if (isLoggedIn) 
      {
        if(lang === 'en'){isConfirmed = window.confirm('Are you sure you want to delete this review?');}
        if(lang === 'es'){isConfirmed = window.confirm('¿Eliminar reseña?');}
        if (isConfirmed) { deleteReview(reviewID); }
      } 
      else 
      {
        if(lang === 'en'){alert('You must sign in as an admin in order to delete reviews');}
        if(lang === 'es'){alert('Debe iniciar sesión como administrador para eliminar resñas');}
        loginOnly();
        isLoggedIn = getIsLoggedIn();
        if(isLoggedIn)
        {
          if(lang === 'en'){isConfirmed = window.confirm('Are you sure you want to delete this review?');}
          if(lang === 'es'){isConfirmed = window.confirm('¿Eliminar reseña?');}
          if (isConfirmed) { deleteReview(reviewID); }
          updatwRawReviews();
        }
      }
    }
  });

async function approveReview(reviewID) {
  try 
  {
    console.log('Hola como andas');
    let rawReviewDocRef;
    if(lang === 'en'){rawReviewDocRef = doc(db,'rawReviews', reviewID);}
    if(lang === 'es'){rawReviewDocRef = doc(db,'rawSpanishReviews', reviewID);}
    const docSnap = await getDoc(rawReviewDocRef);
    const approvedReview = docSnap.data();
    await deleteDoc(rawReviewDocRef);
    await addDoc(colRef, {
      name: approvedReview.name,
      phone: approvedReview.phone,
      email: approvedReview.email,
      message: approvedReview.message
    })
    .then(() => {
      if(lang === 'en'){alert("Review approved and moved to the public collection.");}
      if(lang === 'es'){alert("Reseña aprobada y trasladada a la colección pública.");}
    });
    await deleteDoc(rawReviewDocRef);
  } catch (error) 
  {
    if (lang === 'en') {alert("Error, could not approve the review.");} 
    else if (lang === 'es') {alert("Error, no se pudo aprobar la reseña.");}
  }
}

// Function to decline a raw review
async function declineReview(reviewID) {
  try {
    await deleteDoc(doc(colRefRaw, reviewID));
    if (lang === 'en') {
      alert("Review declined and deleted from the raw collection.");
    } else if (lang === 'es') {
      alert("Reseña rechazada y eliminada de la colección en crudo.");
    }
  } catch (error) {
    if (lang === 'en') {
      alert("Error, could not decline the review.");
    } else if (lang === 'es') {
      alert("Error, no se pudo rechazar la reseña.");
    }
  }
}

  // Event listener for the approve and decline buttons on raw reviews
  rawReviewsContainer.addEventListener("click", (event) => {
    const target = event.target;
    if (target && target.matches("input[data-review-id]")) {
      const reviewID = target.getAttribute("data-review-id");
      if (target.value === approveButtonText) {
        approveReview(reviewID);
      } else if (target.value === declineButtonText) {
        declineReview(reviewID);
      }
    }
  });

});
