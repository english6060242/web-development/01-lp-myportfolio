// Initialize isLoggedIn with the stored value or default to false
let isLoggedIn = localStorage.getItem('isLoggedIn') === 'true' || false;

// Getter function
export function getIsLoggedIn() {
  return isLoggedIn;
}

const localUser = {
  username: 'admin',
  password: 'admin',
};

const lang = document.documentElement.lang;

// Function to update the login text
export function updateLoginText() {
  let login_out_text;
  const loginText = document.getElementById("login_text");
  if (isLoggedIn) {
    if(lang === 'en'){loginText.textContent = "Admin Logout";}
    if(lang === 'es'){loginText.textContent = "Admin - Salir";}

  } else {
    if(lang === 'en'){loginText.textContent = "Admin Login";}
    if(lang === 'es'){loginText.textContent = "Admin - Acceso";}
  }
}

export function loginOnly() {
  let username;
  if(lang === 'en'){username = prompt('Enter your username:');}
  if(lang === 'es'){username = prompt('Ingrese su usuario:');}
  if(username != null) 
  {
    let password;
    if(lang === 'en'){password = prompt('Enter your password:');}
    if(lang === 'es'){password = prompt('Ingrese su constraseña:');}
    if (username === localUser.username && password === localUser.password) {
      isLoggedIn = true;
      localStorage.setItem('isLoggedIn', isLoggedIn);
      updateLoginText();
      if(lang === 'en'){alert('Authentication successful!');}
      if(lang === 'es'){alert('¡Acceso concedido!');}
    } 
    else
    { 
      if(lang === 'en'){alert('Authentication failed. Please check your username and password.');}
      if(lang === 'es'){alert('Acceso denegado, revise su usuario y/o contraseña.');}
    }
  }
  else
  {
    if(lang === 'en'){alert('Authentication failed. Canceled by user.');}
    if(lang === 'es'){alert('Falla en la autenticación. Cancelada por el usuario.');}
  }
}

// Function to handle login/logout
export function handleLoginLogout() {
  const loginbutton = document.getElementById("authenticate");

  if (isLoggedIn) {
    let confirmLogout;
    if(lang === 'en'){confirmLogout = confirm("Are you sure you want to log out?");}
    if(lang === 'es'){confirmLogout = confirm("¿Desea Salir?");}
    if (confirmLogout) {
      isLoggedIn = false;
      localStorage.setItem('isLoggedIn', isLoggedIn);
      updateLoginText();
      if(lang === 'en'){alert('You have been logged out.');}
      if(lang === 'es'){alert('Su sesión de administrador ha sido cerrada.');}
    }
  } else {
    let username;
    if(lang === 'en'){username = prompt('Enter your username:');}
    if(lang === 'es'){username = prompt('Ingrese su usuario:');}
    if(username != null) 
    {
      let password;
      if(lang === 'en'){password = prompt('Enter your password:');}
      if(lang === 'es'){password = prompt('Ingrese su contraseña:');}
      if (username === localUser.username && password === localUser.password) {
        isLoggedIn = true;
        localStorage.setItem('isLoggedIn', isLoggedIn);
        updateLoginText();
        if(lang === 'en'){alert('Authentication successful!');}
        if(lang === 'es'){alert('¡Acceso concedido!');}
      } 
      else
      { 
        if(lang === 'en'){alert('Authentication failed. Please check your username and password.');}
        if(lang === 'es'){alert('Acceso denegado, revise su usuario y/o contraseña.');}
      }
    }
    else 
    {
      if(lang === 'en'){alert('Authentication failed. Canceled by user.');}
      if(lang === 'es'){alert('Falla en la autenticación. Cancelada por el usuario.');}
    }
  }
}